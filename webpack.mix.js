const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css');

mix.setResourceRoot('/');

mix.copyDirectory('resources/images', 'public/images');

mix.copyDirectory('resources/fonts', 'public/fonts');
mix.copyDirectory('resources/js/script.js', 'public/js');
mix.copyDirectory('resources/js/jquery-3.6.0.min.js', 'public/js');


mix.sass('resources/sass/main.scss', 'public/css').options({
    processCssUrls: false
});

mix
  .js(["resources/js/admin/admin.js"], "public/js")
  .sass("resources/sass/admin/admin.scss", "public/css")
  .vue();

if (mix.inProduction()) {
  mix.version();
}
