<?php

namespace App\Http\Controllers;

use App\Models\Review;
use App\Models\Test;
use Illuminate\Http\Request;

class MainController extends Controller
{
    function index(){

        $reviews = Review::all();
        $questions = Test::orderBy('order')->get();
        return view('home', [
            'reviews' => $reviews,
            'questions' => $questions
        ]);
    }
}
