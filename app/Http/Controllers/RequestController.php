<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;

class RequestController extends Controller
{
    function index( Request $request ){

        $success = false;
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'agreenment' => 'required|in:1'
        ]);
        
        $query = [
            'fields' => [
                'TITLE' => 'Заявка с Сайта '.$request->get('name'),
                'NAME' => $request->get('name'),
                'SECOND_NAME' => ' ',
                'LAST_NAME' => ' ',
                'PHONE' => [
                    [
                        'VALUE' => $request->get('phone'), 'VALUE_TYPE' => 'MOBILE'
                    ]
                ],
                'ASSIGNED_BY_ID' => 19,
                'UF_CRM_1525667771' => 1195
            ]
        ];
        $result = self::sendRequest($_ENV['API_URL'], 'crm.lead.add', $query, 'POST');
        if( $result != false && !empty($result) && !empty($result->result) ){
            $success = true;
        }
        return response()->json(
            [
                'success' => $success,
            ]
        );
    }

        /**
     * Sernding request to api
     * @param String $url - api url
     * @param Sitrng $method - name of method
     * @param Array $query - query of request
     * @param String $method_type - request method type
     * @param Int $delay - request delay in seconds
     * @return Bool|Object
     */
    static function sendRequest($url, $method, $query, $method_type = 'GET', $delay = 0){
        $client = new Client();
        $res = null;
        switch ($method_type) {
            case 'POST':
                try {
                    $res = $client->request($method_type, $url.$method.'.json', [
                        'form_params' => $query,
                        'delay' => $delay*1000
                    ]);
                    break;
                } catch ( ClientException $exception) {
                    Log::info([$method_type, $url.$method.'.json', $query]);
                    $response = $exception->getResponse();
                    $responseBodyAsString = $response->getBody()->getContents();
                    Log::error($responseBodyAsString);
                    return false;
                }
            default:
                try {
                    $res = $client->request($method_type, $url.$method.'.json', [
                        'query' => $query,
                        'delay' => $delay*1000
                    ]);
                    break;
                } catch ( ClientException $exception) {
                    Log::info([$method_type, $url.$method.'.json', $query]);
                    $response = $exception->getResponse();
                    $responseBodyAsString = $response->getBody()->getContents();
                    Log::error($responseBodyAsString);
                    return false;
                }
        }
        if( is_object($res) && $res->getStatusCode() == 200 ){
            return json_decode($res->getBody());
        }
        return false;
    }
}
