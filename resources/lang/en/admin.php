<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
                
            //Belongs to many relations
            'roles' => 'Roles',
                
        ],
    ],

    'review' => [
        'title' => 'Reviews',

        'actions' => [
            'index' => 'Reviews',
            'create' => 'New Review',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'text' => 'Text',
            
        ],
    ],

    'test' => [
        'title' => 'Tests',

        'actions' => [
            'index' => 'Tests',
            'create' => 'New Test',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'question' => 'Question',
            
        ],
    ],

    'test' => [
        'title' => 'Tests',

        'actions' => [
            'index' => 'Tests',
            'create' => 'New Test',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'order' => 'Order',
            'question' => 'Question',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];