$(window).scroll(function() {
    if( $(window).scrollTop() >=  $('header').height() + 5 ){
        $('header').addClass('sticky')
        $('main').addClass('sticky')
    }else if( $(window).scrollTop() <=  $('header').height() - 5 ){
        $('header').removeClass('sticky')
        $('main').removeClass('sticky')

    }

    if( $('#first_screen') ){
        if( $(window).scrollTop() <= $('#first_screen').height() ){
            $('.first_screen__person-image__honeycomb').css('top', (-7+$(window).scrollTop()*0.02)+'%')
        }
    }

    if( $('#third_screen') ){
        if( $(window).scrollTop() <= $('#third_screen').offset().top + $('#third_screen').height() ){
            $('.third_screen__person-image__honeycomb').css('top', (-9+$(window).scrollTop()*0.015)+'%')
        }
    }

    if( $('#eight_screen')){
        if( $(window).scrollTop() <= $('#eight_screen').offset().top + $('#eight_screen').height() ){
            $('.eight_screen__image-honeycomb').css('top', (-30-$(window).scrollTop()*0.0035)+'%')
        }
    }
})

$(document).ready(function(){
    $('.reviews').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 4,
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '60px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '60px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ],
        prevArrow: $('.reviews__nav .prev'),
        nextArrow: $('.reviews__nav .next'),
    });

});

$(window).resize(function() {
    if( $(window).width() >= 767 ){
        closeBurger()
    }
})


const showBurger = () => {
    $('#burger').addClass('show')
}

const closeBurger = () => {
    $('#burger').removeClass('show')
}

const showModal = () => {
    var modal = new bootstrap.Modal('#modal')
    modal.toggle()
}

const showTestModal = () => {
    var modal = new bootstrap.Modal('#test_modal')
    modal.toggle()
}
