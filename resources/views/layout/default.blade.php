@include('layout.header', ['inner' => $inner ?? false])
@yield('body')
@include('layout.footer', ['inner' => $inner ?? false])
