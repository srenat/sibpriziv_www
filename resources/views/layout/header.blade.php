<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    {{-- <link rel="manifest" href="/manifest.json"> --}}
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Bootstrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <!-- Bootstrap 5 -->

    <script src="/js/jquery-3.6.0.min.js"></script>

    {{-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> --}}

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <link rel="stylesheet" href="/css/main.css">
    <script src="/js/script.js"></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TDWCD8F');</script>
    <!-- End Google Tag Manager -->

    <script src="//st.yagla.ru/js/y.c.js?h=80ee0697168d2c9a50df1a807af7c995"></script>
</head>
<body>
    <script> $(document).ready(function(){ $('form').on('submit', function(){ if ( typeof yaglaaction == "function" ) { yaglaaction('sendform');}});}); </script>
    <script> $(document).ready(function(){ $('a').on('click', function(){ if ( typeof yaglaaction == "function" ) {
    yaglaaction('click');}});}); </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDWCD8F"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <header class="">
        <div class="header container">
            <a href="javascript:void(0)" onclick="showBurger()" class="header__burger">
                <img src="/images/icons/burger.svg"/>
            </a>
            <div  class="header__logo">
                <a href="/">
                    <img src="/images/logo.png"/>
                </a>
            </div>
            <nav class="header__menu">
                <a class="header__menu-item" href="@if( $inner == true )/@endif#about">
                    О компании
                </a>
                <a class="header__menu-item" href="@if( $inner == true )/@endif#price">
                    Цены
                </a>
                <a class="header__menu-item" href="@if( $inner == true )/@endif#services">
                    Услуги
                </a>
                <a class="header__menu-item" href="@if( $inner == true )/@endif#faq">
                    Вопросы и ответы
                </a>
            </nav>
            <div class="header__contacts">
                <a class="header__contacts-phone" href="tel:83832774656">
                    8 (383) 277-46-56
                </a>
                <div class="header__contacts-location">
                    <img src="/images/icons/location.svg"/>
                    <span>Новосибирск</span>
                </div>
            </div>
            <div class="header__actions">
                <a href="javascript:void(0)" class="header__actions-callback" onclick="showModal()">
                    Заказать обратный звонок
                </a>
                <div class="header__actions-socials">
                    <a href="https://wa.me/79537977957" target="_blank">
                        <img src="/images/icons/whatsapp.svg"/>
                    </a>
                    <a href="tg://resolve?domain=Sibprizyv" target="_blank">
                        <img src="/images/icons/telegram.svg"/>
                    </a>
                    <a href="viber://chat?number=%2B79537977957" target="_blank">
                        <img src="/images/icons/viber.svg"/>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <div id="app">
    <main>
