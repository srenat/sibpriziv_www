</main>
<!-- Modal -->
<modal-request-component></modal-request-component>

<!-- Test Modal -->
<modal-test-component :questions="{{ json_encode($questions ?? [])}}"></modal-test-component>
</div>
<footer>
    <div class="footer__advantages advantages container">
        <div class="advantages__item">
            <div class="advantages__item-icon full">
                <img src="/images/icons/advantages/advantage_5.svg"/>
            </div>
            <div class="advantages__item-title">
                По договору
            </div>
            <div class="advantages__item-description">
                Вы заключаете официальный договор с нашей компанией
            </div>
        </div>
        <div class="advantages__item">
            <div class="advantages__item-icon full">
                <img src="/images/icons/advantages/advantage_6.svg"/>
            </div>
            <div class="advantages__item-title">
                От 11 000 ₽ в месяц
            </div>
            <div class="advantages__item-description">
                Вы можете оплачивать услуги в рассрочку помесячно
            </div>
        </div>
        <div class="advantages__item">
            <div class="advantages__item-icon full">
                <img src="/images/icons/advantages/advantage_7.svg"/>
            </div>
            <div class="advantages__item-title">
                Законно
            </div>
            <div class="advantages__item-description">
                Ваши дела ведут профессиональные юристы по военному праву
            </div>
        </div>
    </div>
    <div class="footer__form container">
        <div class="form">
            <div class="form__title">
                Бесплатная консультация по получению военного билета
            </div>
            <div id="footer_form">
            <footer-request-component></footer-request-component>
            </div>
        </div>
    </div>
    <div class="footer__map">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A3f43f2d81079eb734713473370c8983006d0b4fd135cbff8375459570f368e6a&amp;width=100%25&amp;height=520&amp;lang=ru_RU&amp;scroll=false">
        </script>
        <div class="footer__map-address container">
            <div class="address">
                <a href="javascript:void(0)" class="button" onclick="showModal()">
                    Запись на бесплатную консультацию
                </a>
                <a href="tel:83832774656" class="address__phone">
                    8 (383) 277-46-56
                </a>

                <div class="address__info">
                    <div class="address__info-description">
                        Офис в Новосибирске
                    </div>
                    <div class="address__info-title">
                        г. Новосибирск<br>
                        пр. Карла Маркса, д. 57<br>
                        офис 320
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__hours container">
        Время работы: Пн.-Пт. с 9:00 до 18:00, без перерыва на обед. Сб.,вск. - выходные дни
    </div>
    <div class="footer__info container">
        <a href="/" class="header__logo">
            <img src="/images/logo.png"/>
        </a>
        <nav class="header__menu">
            <a class="header__menu-item" href="@if( $inner == true )/@endif#about">
                О компании
            </a>
            <a class="header__menu-item" href="@if( $inner == true )/@endif#price">
                Цены
            </a>
            <a class="header__menu-item" href="@if( $inner == true )/@endif#services">
                Услуги
            </a>
            <a class="header__menu-item" href="@if( $inner == true )/@endif#faq">
                Вопросы и ответы
            </a>
        </nav>
        <div class="header__contacts">
            <a class="header__contacts-phone" href="tel:83832774656">
                8 (383) 277-46-56
            </a>
            <a class="header__contacts-email" href="mailto:sibir.p.2021@gmail.com">
                sibir.p.2021@gmail.com
            </a>
        </div>
        <div class="header__actions">
            <a href="javascript:void(0)" class="header__actions-callback" onclick="showModal()">
                Заказать обратный звонок
            </a>
            <div class="header__actions-socials">
                <a href="https://wa.me/79537977957" target="_blank">
                    <img src="/images/icons/whatsapp.svg"/>
                </a>
                <a href="tg://resolve?domain=Sibprizyv" target="_blank">
                    <img src="/images/icons/telegram.svg"/>
                </a>
                <a href="viber://chat?number=%2B79537977957" target="_blank">
                    <img src="/images/icons/viber.svg"/>
                </a>
            </div>
        </div>
    </div>
    <div class="footer__description container">
        Внимание! Решение об освобождении, призыве на военную службу или предоставлении отсрочки от исполнения воинской обязанности принимается исключительно призывной комиссией. Сайт не пропагандирует уклонение от военной службы и совершение действий, запрещенных действующим законодательством. На сайте содержится информация о работе юридических компаний, юристов и адвокатов, работающих в сфере военного права.
        <br>
        <br>
        Данный интернет-сайт носит исключительно информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями статьи 437 (п. 2) Гражданского кодекса Российской Федерации. Для получения подробной информации о наличии и стоимости указанных услуг, пожалуйста, обращайтесь по указанным контактам.
    </div>
    <div class="footer__copyrights container">
        <span>
            © Сибирский призывник {{date('Y')}} | Все права защищены
        </span>
        <a href="/privacy-policy">
            Политика конфиденциальности
        </a>
    </div>
</footer>
<section id="burger">
    <a class="burger__close" href="javascript:void(0)" onclick="closeBurger()">
        <img src="/images/icons/close.svg"/>
    </a>
    <div class="burger__logo">
        <a href="/">
            <img src="/images/logo.png"/>
        </a>
    </div>
    <div class="burger__menu">
        <a class="burger__menu-item" href="@if( $inner == true )/@endif#about" onclick="closeBurger()">
            О компании
        </a>
        <a class="burger__menu-item" href="@if( $inner == true )/@endif#price" onclick="closeBurger()">
            Цены
        </a>
        <a class="burger__menu-item" href="@if( $inner == true )/@endif#services" onclick="closeBurger()">
            Услуги
        </a>
        <a class="burger__menu-item" href="@if( $inner == true )/@endif#faq" onclick="closeBurger()">
            Вопросы и ответы
        </a>
    </div>
    <div class="burger__contacts">
        <a class="burger__contacts-phone" href="tel:83832774656">
            8 (383) 277-46-56
        </a>
        <a class="burger__contacts-email" href="mailto:sibir.p.2021@gmail.com">
            sibir.p.2021@gmail.com
        </a>
        <div class="burger__contacts-location">
            <img src="/images/icons/location.svg"/>
            <span>Новосибирск</span>
        </div>
    </div>
    <div class="burger__actions">
        <a href="javascript:void(0)" class="burger__actions-callback" onclick="showModal()">
            Заказать обратный звонок
        </a>
        <div class="burger__actions-socials">
            <a href="https://wa.me/79537977957" target="_blank">
                <img src="/images/icons/whatsapp.svg"/>
            </a>
            <a href="tg://resolve?domain=Sibprizyv" target="_blank">
                <img src="/images/icons/telegram.svg"/>
            </a>
            <a href="viber://chat?number=%2B79537977957" target="_blank">
                <img src="/images/icons/viber.svg"/>
            </a>
        </div>
    </div>
</section>
    <script src="{{ mix('js/app.js') }}" defer></script>
    {{-- <script src="/js/app.js" defer></script> --}}
</body>
</html>
