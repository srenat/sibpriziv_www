@if( !empty($review))
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Review::class)->getMediaCollection('photo'),
        'media' => $review->getThumbs200ForCollection('photo'),
        'label' => 'Photo'
    ])
@else
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Review::class)->getMediaCollection('photo'),
        'label' => 'Photo'
    ])
@endif


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-left" :class="isFormLocalized ? 'col-md-12' : 'col-md-12'">{{ trans('admin.review.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-12' : 'col-md-12 col-xl-12'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.review.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('text'), 'has-success': fields.text && fields.text.valid }">
    <label for="text" class="col-form-label text-md-left" :class="isFormLocalized ? 'col-md-12' : 'col-md-12'">{{ trans('admin.review.columns.text') }}</label>
        <div :class="isFormLocalized ? 'col-md-12' : 'col-md-12 col-xl-12'">
        <div>
            <wysiwyg v-model="form.text" v-validate="''" id="text" name="text" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('text')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('text') }}</div>
    </div>
</div>


