@extends('layout.default')

@section('title')
    Сибирский призывник
@endsection

@section('body')
    <section id="first_screen">
        <div class="first_screen container">
            <div class="first_screen__texts">
                <h1>
                    Помощь в освобождении от призыва
                </h1>
                <div class="first_screen__texts-title">
                    Получение военного билета – законно и с гарантией результата. Мы прописываем в договоре сроки исполнения обязательств. Если мы их нарушим, вернем вам деньги и выплатим 15 000 ₽ неустойки*
                </div>
                <div class="first_screen__texts-description">
                    *Договор заключается только после медицинского осмотра.
                </div>
            </div>
            <div class="first_screen__person">
                <div class="first_screen__person-image">
                    <div class="first_screen__person-image__honeycomb">
                        <img src="/images/elements/honeycomb.png"/>
                    </div>
                    <img src="/images/persons/main_person.png"/>
                </div>
                <div class="first_screen__person-review">
                    <div class="first_screen__person-review__title">
                        Денис
                    </div>
                    <div class="first_screen__person-review__description">
                        Обратился в компанию за помощью в получении военника, военный билет получил, спасибо! Компании благодарен за поддержку все это время (начиная от руководства и заканчивая юристами), не пожалел что обратился именно к ним.
                    </div>
                    <a href="#reviews" class="first_screen__person-review__link">
                        Читать все отзывы
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="container form_screen">
        <div class="form">
            <div class="form__title">
                <div class="desktop">
                    Хочешь так же? Начни с бесплатной консультации специалиста прямо сейчас!
                </div>
                <div class="mobile">
                    Бесплатная консультация по получению военного билета
                </div>
            </div>
            <request-component></request-component>
        </div>
    </section>
    <section id="second_screen">
        <div class="advantages container">
            <div class="advantages__item">
                <div class="advantages__item-icon">
                    <img src="/images/icons/advantages/advantage_1.svg"/>
                </div>
                <div class="advantages__item-description">
                    Сопровождаем призывника в военкомате и помогаем пройти призывные мероприятия
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__item-icon">
                    <img src="/images/icons/advantages/advantage_2.svg"/>
                </div>
                <div class="advantages__item-description">
                    Работаем до фактического получения призывником военного билета или отсрочки от армии
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__item-icon">
                    <img src="/images/icons/advantages/advantage_3.svg"/>
                </div>
                <div class="advantages__item-description">
                    Обеспечиваем полную юридическую безопасность призывника
                </div>
            </div>
        </div>
    </section>
    <section id="third_screen">
        <div class="third_screen container" id="about">
            <h2>
                Мы дружим с законом!
            </h2>
            <div class="third_screen__person">
                <div class="third_screen__person-image">
                    <div class="third_screen__person-image__honeycomb">
                        <img src="/images/elements/honeycomb1.png"/>
                    </div>
                    <img src="/images/persons/second_person.png"/>
                </div>
                <div class="third_screen__person-title">
                    Реальные <a href="#reviews">отзывы</a> о нашей работе
                </div>
            </div>
            <div class="third_screen__info">
                <div class="description">
                    Мы работаем в строгом соответствии с УК РФ. Мы не выдаем поддельных медицинских карт, военных билетов, и не проворачиваем иных незаконных схем.
                    <br>
                    <br>
                    В основе нашей деятельности – <b>опыт и профессионализм юристов и врачей с многолетней практикой в военном праве.</b> Вам решать:
                    <br>
                    <br>
                    <ul>
                        <li>
                            Самостоятельно проходить все инстанции, связанные с получением отсрочки или военного билета.
                        </li>
                        <li>
                            Довериться нашим специалистам, которые знают обо всех подводных камнях российского законодательства.
                        </li>
                    </ul>
                </div>
                <div class="third_screen__info-form">
                    <div class="third_screen__info-form__left">
                        <a href="javascript:void(0)" class="button" onclick="showModal()">
                            Бесплатная консультация специалиста
                        </a>
                        <div>
                            Перезвоним в течение 1 минуты
                        </div>
                    </div>
                    <div class="third_screen__info-form__right">
                        <h4>
                            Тест на годность к воинской службе
                        </h4>
                        <div>
                            Пройдите короткий тест и узнайте, как можно законно избежать воинской службы. <b>Результат через 1 минуту с точностью 98 %.</b>
                        </div>
                        <a href="javascript:void(0)" class="button button__empty" onclick="showTestModal()">
                            Пройти тест
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="fourth_screen">
        <div class="fourth_screen container" id="services">
            <h2>
                Помощь призывникам Новосибирска по <br>8 направлениям
            </h2>

            <div class="ways">
                <div class="ways__item">
                    <span>Бесплатная консультация по призыву</span>
                </div>
                <div class="ways__item">
                    <span>Помощь в получении военного билета</span>
                </div>
                <div class="ways__item">
                    <span>Отсрочка <br>на 1 призыв</span>
                </div>
                <div class="ways__item">
                    <span>Военный билет <br>после 27 лет</span>
                </div>
                <div class="ways__item">
                    <span>Выявление категории годности призывника</span>
                </div>
                <div class="ways__item">
                    <span>Первичная постановка на воинский учет</span>
                </div>
                <div class="ways__item">
                    <span>Обжалование решений призывной комиссии</span>
                </div>
                <div class="ways__item">
                    <span>Юридическое сопровождение в военкомате</span>
                </div>
            </div>
        </div>
    </section>
    <section id="fifth_screen">
        <div class="fifth_screen container">
            <div class="fifth_screen__title">
                <h2>
                    5 причин
                </h2>
                обратиться в нашу компанию
            </div>
            <div class="reason">
                <div class="reason__number reason__number-1">
                    1
                </div>
                <div class="reason__title">
                    Личное присутствие врачей и юристов в Новосибирске
                </div>
                <div class="reason__description">
                    Наш новосибирский офис открыт для вас ежедневно. Специалисты нашей компании сами подготовят весь пакет документов и будут представлять ваши интересы в суде. Но по вашему желанию мы можем оказать вам помощь дистанционно, используя электронные средства связи.
                </div>
            </div>
            <div class="reason">
                <div class="reason__number reason__number-2">
                    2
                </div>
                <div class="reason__title">
                    Прозрачные цены
                </div>
                <div class="reason__description">
                    Стоимость наших услуг четко прописана в договоре и неизменна. Мы работаем с финансовой гарантией результата. Это значит, что вы выплачиваете нам гонорар только за положительное решение об освобождении от призыва или после получения военного билета.
                </div>
            </div>
            <div class="reason">
                <div class="reason__number reason__number-3">
                    3
                </div>
                <div class="reason__title">
                    Только законный военный<br> билет от военкомата
                </div>
                <div class="reason__description">
                    Мы работаем по законодательству! Мы не занимаемся выдачей компрометирующих документов, которые могут оказать негативное влияние на вашу дальнейшую карьеру.
                </div>
            </div>
            <div class="reason">
                <div class="reason__number reason__number-4">
                    4
                </div>
                <div class="reason__title">
                    Никаких ограничивающих медицинских показаний
                </div>
                <div class="reason__description">
                    Наши врачи прекрасно ориентируются в Расписании болезней. Они не допустят, чтобы вы были освобождены от воинской службы по медицинским показаниям, препятствующим каким-либо видам деятельности – например, вождению автомобиля.
                </div>
            </div>
            <div class="reason">
                <div class="reason__number reason__number-5">
                    5
                </div>
                <div class="reason__title">
                    Финансовая гарантия<br> результата в Соглашении
                </div>
                <div class="reason__description">
                    В договоре прописана гарантия результата и выплаты неустойки в случае нарушения или невыполнения условий с нашей стороны. Мы уверены в качестве своей работы: наших клиентов ни разу не призывали в армию.
                </div>
            </div>
        </div>
    </section>
    <section id="sixth_screen">
        <div class="sixth_screen__advantage container">
            <div class="advantages__item">
                <div class="advantages__item-icon">
                    <img src="/images/icons/advantages/advantage_4.svg"/>
                </div>
                <div class="advantages__item-description">
                    Мы анализируем состояние здоровья призывника по <b>84 параметрам.</b>
                    <br>
                    Сопоставляем полученные данные с <b>2063 диагнозами</b> из расписания болезней на возможность освобождения от армии.
                </div>
            </div>
        </div>
        <div class="sixth_screen__owner container">
            <div class="sixth_screen__owner-person">
                <div class="sixth_screen__owner-person__image">
                    <div class="third_screen__person-image__honeycomb honeycomb" style="top: 14.97%;">
                        <img src="/images/elements/honeycomb1.png">
                    </div>
                    <img src="/images/persons/owner.png"/>
                </div>
                <div class="sixth_screen__owner-person__name">
                    Попов Михаил Владимирович
                </div>
                <div class="sixth_screen__owner-person__title">
                    Руководитель компании
                </div>
            </div>
            <div class="sixth_screen__owner-info">
                <div class="subtitle">
                    Поможем пройти обследования в государственных клиниках и выявим непризывное заболевание. <br>Это гарантирует получение военного билета.
                </div>
                <h3>
                    Пройдите тест и узнайте годны ли <br>вы к службе в армии
                </h3>
                <div class="sixth_screen__owner-info__test">
                    <div class="sixth_screen__owner-info__test-title">
                        Ответьте на 5 простых вопросов и получите заключение специалиста о своих шансах <b>(с вероятностью 98%)</b> НЕ ПОЙТИ в армию на законных основаниях.
                    </div>
                    <a href="javascript:void(0)" class="button" onclick="showTestModal()">
                        Пройти тест
                    </a>
                </div>
                <div class="description">
                    Согласно закону <b>«О воинской обязанности и военной службе»</b> от призыва на военную службу освобождаются лица, признанные ограниченно годными по состоянию здоровья. У большинства призывников при независимом полном медицинском обследовании можно выявить болезни, которые не совместимы со службой в армии. Мы предлагаем вам пройти небольшой тест, чтобы понять сможете ли вы законно получить военный билет по медицинским показателям.
                </div>
            </div>
        </div>
        <div class="sixth_screen__promo container">
            <h2>
                Ни один наш клиент не ушёл в армию
            </h2>
            <h3>
                Военный билет у вас на руках в точные сроки
            </h3>
            <div class="title">
                Начните с бесплатной консультации специалиста прямо сейчас!
            </div>
            <div class="sixth_screen__promo-action">
                <a href="javascript:void(0)" class="button" onclick="showModal()">
                    Бесплатная консультация
                </a>
                <div class="description">
                    Перезвоним в течение 1 минуты
                </div>
            </div>
        </div>
        <div class="sixth_screen__services container" id="price">
            <h2>Наши цены и пакеты услуг</h2>
            <div class="services">
                <div class="services__item">
                    <div class="services__item-title">
                        <span>Отсрочка <br>на 1 призыв</span>
                    </div>
                    <div class="services__item-price">
                        от 20 000 ₽
                    </div>
                    <div class="services__item-description">
                        Оказываем правовое сопровождение обеспечивающее защиту от незаконной отправки в армию до конца текущего или ближайшего призыва
                    </div>
                    <a href="javascript:void(0)" class="button" onclick="showModal()">
                        Получить консультацию
                    </a>
                </div>
                <div class="services__item">
                    <div class="services__item-title">
                        <span>Военный билет</span>
                    </div>
                    <div class="services__item-price">
                        от 50 000 ₽
                    </div>
                    <div class="services__item-description">
                        Для тех, кто хочет законно освободиться от призыва в армию и получить военный билет на законных основаниях
                    </div>
                    <a href="javascript:void(0)" class="button" onclick="showModal()">
                        Получить консультацию
                    </a>
                </div>
                <div class="services__item">
                    <div class="services__item-title">
                        <span>Последний шанс</span>
                    </div>
                    <div class="services__item-price">
                        от 75 000 ₽
                    </div>
                    <div class="services__item-description">
                        Сопровождение призывника до получения военного билета, даже если признали годным и вручили "боевую повестку"
                    </div>
                    <a href="javascript:void(0)" class="button" onclick="showModal()">
                        Получить консультацию
                    </a>
                </div>
                <div class="services__item">
                    <div class="services__item-title">
                        <span>Юридическое сопровождение</span>
                    </div>
                    <div class="services__item-price">
                        от 40 000 ₽
                    </div>
                    <div class="services__item-description">
                        Оказываем полное юридическое сопровождение призывника и консультируем по результатам обследования
                    </div>
                    <a href="javascript:void(0)" class="button" onclick="showModal()">
                        Получить консультацию
                    </a>
                </div>
            </div>
        </div>
        <div class="sixth_screen__installment container">
            <h2>Нет всей суммы сразу? Оплатите в рассрочку!</h2>
            <div class="sixth_screen__installment-info">
                <div class="sixth_screen__installment-info__list">
                    <div class="sixth_screen__installment-info__list-item">
                        <span class="large">55 000 ₽</span> договор в рассрочку на <span class="large">5 месяцев</span>
                    </div>

                </div>
                <div class="sixth_screen__installment-info__description">
                    Хотите узнать подробнее об условиях рассрочки? Закажите обратный звонок! Наш специалист перезвонит, бесплатно проконсультирует вас и расскажет детали сотрудничества.
                </div>
                <div class="sixth_screen__installment-info__action">
                    <a href="javascript:void(0)" class="button" onclick="showModal()">
                        Бесплатная консультация
                    </a>
                    <div class="description">
                        Перезвоним в течение 1 минуты
                    </div>
                </div>
            </div>
            <div class="sixth_screen__installment-percent">
                <h3>
                    Без переплат!
                </h3>
                <h4>
                    <span class="large">11 000 ₽</span> в  месяц
                </h4>
                <div class="sixth_screen__installment-percent__description">
                    Получите <b>военный билет законно</b>, оплатив услугу помощи привзывникам в рассрочку
                </div>
            </div>
        </div>
    </section>
    <section id="eight_screen">
        <div class="eight_screen container">
            <div class="eight_screen__info">
                <div class="eight_screen__info-title">
                    <b>Зачисление в запас</b> и возможность получения законного <b>военного билета</b> —
                </div>
                <div class="eight_screen__info-description">
                    закономерный результат кропотливой работы наших специалистов.
                    <br>
                    <br>
                    Обращаясь к нам, призывники получают главное — <b>юридическую безопасность</b> на всех этапах призывных мероприятий с личным сопровождением наших юристов!
                </div>
            </div>
            <div class="eight_screen__image">
                <div class="eight_screen__image-honeycomb">
                    <img src="/images/elements/honeycomb2.png"/>
                </div>
                <img src="/images/tickeck.png"/>
            </div>
        </div>
    </section>
    <section id="ninth_screen">
        <div class="ninth_screen__attention container">
            <div class="ninth_screen__attention-block">
                <div class="ninth_screen__attention-description">
                    Не исключено, что вы можете купить военный билет в Новосибирске, дав взятку должностным лицам, например, в военкомате или на медицинском обследовании.
                </div>
                <div class="ninth_screen__attention-title">
                    Но мы категорически не рекомендуем этого делать.
                </div>
                <div class="ninth_screen__attention-info">
                    Во-первых, потому что это <b>незаконно</b> и грозит  <b>свободы сроком до 8 лет,</b> не говоря уже о том, что незаконное решение отменят.
                    <br>
                    <br>
                    Во-вторых, потому что нет никаких гарантий, что вы просто не лишитесь своих денег, <b>не получив нужного вам результата.</b>
                    <br>
                    <br>
                    В-третьих, скорей всего это обойдется вам <b>гораздо дороже,</b> чем наши услуги.
                </div>
                <div class="ninth_screen__attention-action">
                    <a href="javascript:void(0)" class="button" onclick="showModal()">
                        Бесплатная консультация
                    </a>
                    <div class="description">
                        Перезвоним в течение 1 минуты
                    </div>
                </div>
            </div>
        </div>
        <div class="ninth_screen__warranty container">
            <h2>Работая с нами  вы гарантировано получите</h2>
            <div class="warranty">
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        1.
                    </div>
                    <div class="warranty__item-title">
                        Возврат средств, в случае недостижения результата
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        2.
                    </div>
                    <div class="warranty__item-title">
                        Оплата услуг помесячно в рассрочку
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        3.
                    </div>
                    <div class="warranty__item-title">
                        Очные консультации специалиста
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        4.
                    </div>
                    <div class="warranty__item-title">
                        Телефонные безлимитные консультации юриста
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        5.
                    </div>
                    <div class="warranty__item-title">
                        Формирование плана проведения медицинского обследования
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        6.
                    </div>
                    <div class="warranty__item-title">
                        Помощь в проведении независимого медицинского обследования
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        7.
                    </div>
                    <div class="warranty__item-title">
                        Оспаривание решений призывных комиссий
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        8.
                    </div>
                    <div class="warranty__item-title">
                        Персональный юрист
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        9.
                    </div>
                    <div class="warranty__item-title">
                        Персональный эксперт по ВВК
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
                <div class="warranty__item">
                    <div class="warranty__item-num">
                        10.
                    </div>
                    <div class="warranty__item-title">
                        Представление интересов в суде
                    </div>
                    <div class="warranty__item-check">

                    </div>
                </div>
            </div>
        </div>
        <div class="ninth_screen__reviews" id="reviews">
            <div class="container">
                <h2>
                    Отзывы
                </h2>
            </div>
            <div id="reviews-block">
                <div class="reviews">
                    @foreach ( $reviews as $review )
                    <div class="reviews__block">
                        <div class="reviews__item">
                            <div class="reviews__item-image">
                                {{-- <img src="{{ $review->getThumbs200ForCollection('photo')[0]['url'] }}"/> --}}
                                <img src="{{ $review->getMedia('photo')[0]->getUrl('main') }}"/>
                            </div>
                            <div class="reviews__item-title">
                                {{ $review->name }}
                            </div>
                            <div class="reviews__item-description">
                                {!! $review->text !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="reviews__nav">
                    <a href="javascript:void(0)" class="prev">
                        <img src="/images/icons/left_arrow.svg"/>
                    </a>
                    <a href="javascript:void(0)" class="next">
                        <img src="/images/icons/right_arrow.svg"/>
                    </a>
                </div>
            </div>
            <div class="container">
                <a href="https://novosibirsk.flamp.ru/addreview/70000001007373828" class="button" target="_blank">
                    Оставить отзыв
                </a>
            </div>
        </div>
    </section>
    <section id="tenth_screen">
        <div class="tenth_screen container" id="faq">
            <h2>
                Отвечаем на ваши вопросы
            </h2>
            <div class="accordion">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                            Вы можете гарантировать успешное решение проблемы при обращении к вам?
                        </button>
                    </h2>
                    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                        <div class="accordion-body">
                            Наши специалисты рекомендуют пройти комплексное обследование с использованием новейших методов диагностики. Если у молодого человека есть хотя бы минимальный шанс не уйти в армию по медицинским показаниям, мы сможем ему помочь. Как я уже упоминал, список болезней, с которым призывник признается не годным к службе в армии, очень обширен.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
                            Когда стоит начать работу над военным билетом?
                        </button>
                    </h2>
                    <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingTwo">
                        <div class="accordion-body">
                            Русская привычка надеяться на «авось» приводит к тому, что призывник обращается к нам, когда проблема уже стоит остро.
                            Дальновидные идут к нам заблаговременно. Такая прозорливость позволит существенно упростить процедуру получения военного билета и, что немаловажно, существенно сэкономить время и средства!
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
                            Каков процент призывников, которые могут по тем или иным причинам получить отсрочку от призыва?
                        </button>
                    </h2>
                    <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingThree">
                        <div class="accordion-body">
                            По статистике 95% обратившихся к нам призывников имеют заболевание, являющееся противопоказанием службе в армии.
                            Соответственно эти 95% и могут получить военный билет на законных медицинских основаниях.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-headingFour">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
                            Можно ли оплатить ваши услуги поэтапно? Существует ли рассрочка?
                        </button>
                    </h2>
                    <div id="panelsStayOpen-collapseFour" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingFour">
                        <div class="accordion-body">
                            В нашей компании существует гибкая система рассрочки сроком до 8 месяцев. Более подробную информацию о видах рассрочки и сроках оплаты можно узнать на бесплатной консультации.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="false" aria-controls="panelsStayOpen-collapseFive">
                            Что будет, если бегать от военкомата до 27 лет?
                        </button>
                    </h2>
                    <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingFive">
                        <div class="accordion-body">
                            В соответствие с поправками в законодательство гражданам, в лучшем случае, «успешно спрятавшимся» и пришедшим в 27 лет за военным билетом в военкомат, будет выдаваться только справка (так называемый «волчий билет»). Соответственно в будущем проблематично будет устроиться на хорошую работу. Для того, чтобы добиться результата, нужно действовать, а не уходить от проблемы. В худшем случае, такие граждане могут быть признаны уклонистами и осуждены
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-headingSix">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSix" aria-expanded="false" aria-controls="panelsStayOpen-collapseSix">
                            Какие существуют законные способы, чтобы не попасть в армию?
                        </button>
                    </h2>
                    <div id="panelsStayOpen-collapseSix" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingSix">
                        <div class="accordion-body">
                            Молодому человеку необходимо знать свои права! Существует целый ряд законных оснований не служить в армии. Учеба в ВУЗе, рождение ребенка, опекунство, но это лишь отсрочка решения проблемы. - Единственным законным основанием для получения военного билета являются медицинские показания.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
